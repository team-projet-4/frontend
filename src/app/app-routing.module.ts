import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import {
  AdminGuard,
  TeacherGuard,
  UserGuard,
  UserGuardMBTI,
} from "@services/frontend/auth.guard";

const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  {
    path: "home",
    loadChildren: () =>
      import("./pages/home/home.module").then((m) => m.HomePageModule),
  },
  {
    path: "admin-login",
    loadChildren: () =>
      import("./pages/admin-login/admin-login.module").then(
        (m) => m.AdminLoginPageModule
      ),
  },
  {
    path: "login",
    loadChildren: () =>
      import("./pages/login/login.module").then((m) => m.LoginPageModule),
  },
  {
    path: "admin-home",
    canActivate: [AdminGuard],
    loadChildren: () =>
      import("./pages/admin-home/admin-home.module").then(
        (m) => m.AdminHomePageModule
      ),
  },
  {
    path: "teacher-home",
    canActivate: [TeacherGuard],
    children: [
      {
        path: "",
        canActivate: [TeacherGuard],
        loadChildren: () =>
          import("./pages/teacher-home/teacher-home.module").then(
            (m) => m.TeacherHomePageModule
          ),
      },
      {
        path: ":courseId",
        canActivate: [TeacherGuard],
        loadChildren: () =>
          import("./pages/team-management/team-management.module").then(
            (m) => m.TeamManagementPageModule
          ),
      },
    ],
  },
  {
    path: "user-home",
    canActivate: [UserGuardMBTI],
    loadChildren: () =>
      import("./pages/user-home/user-home.module").then(
        (m) => m.UserHomePageModule
      ),
  },
  {
    path: "user-suggestion",
    canActivate: [UserGuardMBTI],
    loadChildren: () =>
      import("./pages/user-suggestion/user-suggestion.module").then(
        (m) => m.UserSuggestionPageModule
      ),
  },
  {
    path: "add-user",
    canActivate: [AdminGuard],
    loadChildren: () =>
      import("./pages/admin-home/add-user/add-user.module").then(
        (m) => m.AddUserPageModule
      ),
  },
  {
    path: "teacher-login",
    loadChildren: () =>
      import("./pages/teacher-login/teacher-login.module").then(
        (m) => m.TeacherLoginPageModule
      ),
  },
  {
    path: "test-mbti",
    canActivate: [UserGuard],
    loadChildren: () =>
      import("./pages/test-mbti/test-mbti.module").then(
        (m) => m.TestMBTIPageModule
      ),
  },
  {
    path: "edit-profile",
    canActivate: [UserGuardMBTI],
    loadChildren: () =>
      import("./pages/edit-profile/edit-profile.module").then(
        (m) => m.EditProfilePageModule
      ),
  },
  {
    path: "edit-teacher-profile",
    canActivate: [TeacherGuard],
    loadChildren: () =>
      import("./pages/edit-teacher-profile/edit-teacher-profile.module").then(
        (m) => m.EditTeacherProfilePageModule
      ),
  },
  {
    path: "create-team-code",
    canActivate: [UserGuardMBTI],
    loadChildren: () =>
      import("./pages/create-team-code/create-team-code.module").then(
        (m) => m.CreateTeamCodePageModule
      ),
  },
  {
    path: "user-infos",
    canActivate: [UserGuardMBTI],
    loadChildren: () =>
      import("./pages/user-infos/user-infos.module").then(
        (m) => m.UserInfosPageModule
      ),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
