import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { TeamManagementPageRoutingModule } from "./team-management-routing.module";
import { DragulaModule } from "ng2-dragula";
import { TeamManagementPage } from "./team-management.page";
import { ComponentsModule } from "@components/components.module";
import { DisplayTeamsComponent } from "@components/display-teams/display-teams.component";
import { EditTeamsComponent } from "@components/edit-teams/edit-teams.component";

@NgModule({
  imports: [
    ComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    TeamManagementPageRoutingModule,
    DragulaModule,
  ],
  declarations: [TeamManagementPage, DisplayTeamsComponent, EditTeamsComponent],
})
export class TeamManagementPageModule {}
