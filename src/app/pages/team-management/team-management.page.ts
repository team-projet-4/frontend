import { CorePage } from "./../../core/core.page";
import { ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-team-management",
  templateUrl: "./team-management.page.html",
  styleUrls: ["./team-management.page.scss"],
})
export class TeamManagementPage extends CorePage implements OnInit {
  constructor(private activatedRoute: ActivatedRoute) {
    super();
  }

  loadedFeature = "ShowTeam";

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }

  ngOnInit() {}
}
