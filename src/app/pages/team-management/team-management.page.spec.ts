import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { TeamManagementPage } from "./team-management.page";

describe("TeamManagementPage", () => {
  let component: TeamManagementPage;
  let fixture: ComponentFixture<TeamManagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TeamManagementPage],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(TeamManagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  // it("should create", () => {
  //   expect(component).toBeTruthy();
  // });
});
