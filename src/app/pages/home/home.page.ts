import { Component, OnInit } from "@angular/core";
import { CorePage } from "@core/core.page";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage extends CorePage implements OnInit {
  constructor() {
    super();
  }

  ngOnInit() {}
}
