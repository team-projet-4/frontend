import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { TeacherHomePageRoutingModule } from "./teacher-home-routing.module";

import { TeacherHomePage } from "./teacher-home.page";
import { NgxDropzoneModule } from "ngx-dropzone";
import { ComponentsModule } from "@components/components.module";
import { UserService } from "@services/api/user.service";
import { PipesModule } from "src/app/pipes/pipes.module";
import { UploadService } from "@services/api/upload.service";
import { GroupService } from "@services/api/group.service";

@NgModule({
  imports: [
    ComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    NgxDropzoneModule,
    TeacherHomePageRoutingModule,
  ],
  providers: [UserService, UploadService, GroupService],
  declarations: [TeacherHomePage],
})
export class TeacherHomePageModule {}
