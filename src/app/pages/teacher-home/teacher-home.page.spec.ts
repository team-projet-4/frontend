import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { TeacherHomePage } from "./teacher-home.page";
import { ComponentsModule } from "@components/components.module";
import { NgxDropzoneModule } from "ngx-dropzone";
import { FormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";
import { Routes } from "@services/api/route.manager";
import { ToasterService } from "@services/frontend/toaster.service";
import { LoadingService } from "@services/frontend/loading.service";
import { PipesModule } from "src/app/pipes/pipes.module";

describe("TeacherHomePage", () => {
  let component: TeacherHomePage;
  let fixture: ComponentFixture<TeacherHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Routes,
          useFactory: (t: ToasterService, l: LoadingService) =>
            new Routes(t, l),
          deps: [ToasterService, LoadingService]
        }
      ],
      declarations: [TeacherHomePage],
      imports: [
        IonicModule.forRoot(),
        PipesModule,
        ComponentsModule,
        NgxDropzoneModule,
        FormsModule,
        RouterTestingModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(TeacherHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
