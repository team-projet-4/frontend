import { Component, OnInit } from "@angular/core";
import { FileUtils } from "@utils/file.utils";
import { UserService } from "@services/api/user.service";
import {
  isCreateAccount,
  IUserInfo,
  IUserGroupSettings,
} from "polyteam-shared";
import { StateService } from "@services/state/state.service";
import { AuthenticationService } from "@services/api/authentication.service";
import { StrictNgForm } from "@core/core.types";
import { LoadingService } from "@services/frontend/loading.service";
import { ToasterService } from "@services/frontend/toaster.service";
import { UploadService } from "@services/api/upload.service";
import { Router } from "@angular/router";
import { GroupService } from "@services/api/group.service";

@Component({
  selector: "app-teacher-home",
  templateUrl: "./teacher-home.page.html",
  styleUrls: ["./teacher-home.page.scss"],
})
export class TeacherHomePage implements OnInit {
  public nbUsers = { lower: 2, upper: 4 };

  constructor(
    private router: Router,
    private userService: UserService,
    private state: StateService,
    private auth: AuthenticationService,
    private loading: LoadingService,
    private toaster: ToasterService,
    private uploadService: UploadService,
    private groupService: GroupService
  ) {}

  public files: File[] = [];
  public teacher: IUserInfo;
  private readonly FILE_NAME_REGEX = /^[A-Z]{3}[0-9]{4}([A-Z]{1})?_[0-9]{2}_[lc]\.csv$/;

  ngOnInit() {
    this.state.user.subscribe((ob) => (this.teacher = ob));
  }

  async uploadProfilePic(files: FileList) {
    await this.loading.wrap(async () => await this._uploadProfilePic(files));
  }

  public badgeColor(percent: number) {
    return percent < 50 ? "danger" : percent < 85 ? "warning" : "success";
  }

  public async createTeams(courseId: string) {
    await this.groupService.createTeams(courseId);
  }

  private async _uploadProfilePic(files: FileList) {
    const profilepic = await this.uploadService.upload(files.item(0));
    const res = await this.userService.updateUser({ profilepic });

    if (res) {
      await this.auth.update("TEACHER");
    }
  }

  async onSelect(event: any) {
    const addedFiles = event.addedFiles as File[];

    if (
      addedFiles.length === 1 &&
      this.FILE_NAME_REGEX.test(addedFiles[0].name)
    ) {
      this.files = event.addedFiles;
    } else {
      await this.toaster.error(
        "Le nom ou le type du fichier n'est pas valide!"
      );
    }
  }

  async upload(form: StrictNgForm<IUserGroupSettings>) {
    await this.loading.wrap(async () => await this._upload(form));
    this.resetForm();
  }

  onRemove(event: any) {
    this.files.splice(this.files.indexOf(event), 1);
  }
  gotoEditTeacherProfile() {
    this.router.navigate(["edit-teacher-profile"]);
  }

  private async _upload(form: StrictNgForm<IUserGroupSettings>) {
    const csv = await FileUtils.readAsText(this.files[0]);
    const [status, users] = await FileUtils.csvToJson(csv, isCreateAccount);

    if (!status) {
      await this.toaster.error("Le contenu du fichier n'est pas valide!");
      return;
    }

    const settings = form.value;
    const acronym = this.files[0].name.split(".")[0];
    const res = await this.userService.uploadList({ acronym, settings, users });

    if (res) {
      await this.auth.update("TEACHER");
    }
  }

  private resetForm() {
    this.files = [];
    this.nbUsers = { lower: 2, upper: 4 };
  }
}
