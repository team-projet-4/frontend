import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { TestMBTIPage } from "./test-mbti.page";
import { FormsModule } from "@angular/forms";
import { Routes } from "@services/api/route.manager";
import { ToasterService } from "@services/frontend/toaster.service";
import { LoadingService } from "@services/frontend/loading.service";
import { RouterTestingModule } from "@angular/router/testing";

describe("TestMBTIPage", () => {
  let component: TestMBTIPage;
  let fixture: ComponentFixture<TestMBTIPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Routes,
          useFactory: (t: ToasterService, l: LoadingService) =>
            new Routes(t, l),
          deps: [ToasterService, LoadingService]
        }
      ],
      declarations: [TestMBTIPage],
      imports: [IonicModule.forRoot(), FormsModule, RouterTestingModule]
    }).compileComponents();

    fixture = TestBed.createComponent(TestMBTIPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
