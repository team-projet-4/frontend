import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { TestMBTIPage } from "./test-mbti.page";

const routes: Routes = [
  {
    path: "",
    component: TestMBTIPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestMBTIPageRoutingModule {}
