import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { TestMBTIPageRoutingModule } from "./test-mbti-routing.module";

import { TestMBTIPage } from "./test-mbti.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestMBTIPageRoutingModule
  ],
  declarations: [TestMBTIPage]
})
export class TestMBTIPageModule {}
