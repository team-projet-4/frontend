import { Component, OnInit, ViewChild } from "@angular/core";
import { IonSlides } from "@ionic/angular";
import { CorePage } from "@core/core.page";
import { Router } from "@angular/router";
import { GroupService } from "@services/api/group.service";
import { LoadingService } from "@services/frontend/loading.service";
import { MBTIAnswerFormPart, MBTIForm, MBTIFormPart } from "polyteam-shared";
import { ToasterService } from "@services/frontend/toaster.service";
import { AuthenticationService } from "@services/api/authentication.service";

@Component({
  selector: "app-test-mbti",
  templateUrl: "./test-mbti.page.html",
  styleUrls: ["./test-mbti.page.scss"],
})
export class TestMBTIPage extends CorePage implements OnInit {
  public MBTIForm: MBTIForm;
  public MBTIAnswerForm: MBTIAnswerFormPart[] = [];
  public MBTIFormPart: MBTIAnswerFormPart;
  public isSliderEnd = false;
  public isSliderStart = true;

  @ViewChild(IonSlides, { static: false }) slides: IonSlides;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private toaster: ToasterService,
    private groupService: GroupService,
    private loading: LoadingService
  ) {
    super();
  }

  ngOnInit() {
    this.toaster.show(
      "Portez une attention particulière au test! Vous n'aurez la chance de le remplir qu'une seule fois."
    );

    this.loading.wrap(async () => await this.init());
  }

  public async next() {
    await this.slides.slideNext();
  }

  public async previous() {
    await this.slides.slidePrev();
  }

  private async updateSlidePosition() {
    this.isSliderEnd = await this.slides.isEnd();
    this.isSliderStart = await this.slides.isBeginning();
  }

  private async init() {
    this.MBTIForm = await this.groupService.requestMBTIForm();
    this.MBTIForm.forEach((p) => {
      this.MBTIAnswerForm.push(this.answerPart(p));
    });

    this.slides.ionSlideDidChange.subscribe(async () =>
      this.updateSlidePosition()
    );
  }

  private answerPart(formPart: MBTIFormPart): MBTIAnswerFormPart {
    return {
      id: formPart.id,
      answers: formPart.questions.map((q) => ({ id: q.id, answer: undefined })),
    };
  }

  public async sendForm() {
    const personalityType = await this.groupService.requestMBTIPersonality(
      this.MBTIAnswerForm
    );

    if (!personalityType) {
      return await this.toaster.error("Veuillez remplir le formulaire.");
    }

    await this.toaster.show(
      "Félicitions votre personalité est : " + personalityType.join("")
    );

    await this.authService.update("USER");
    await this.router.navigate(["user-infos"]);
  }
}
