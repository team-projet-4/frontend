import { Component, OnInit, Input } from "@angular/core";
import { UserService } from "@services/api/user.service";
import { IUserInfo } from "polyteam-shared";
import { StateService } from "@services/state/state.service";
import { AuthenticationService } from "@services/api/authentication.service";
import { LoadingService } from "@services/frontend/loading.service";
import { UploadService } from "@services/api/upload.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-edit-teacher-profile",
  templateUrl: "./edit-teacher-profile.page.html",
  styleUrls: ["./edit-teacher-profile.page.scss"],
})
export class EditTeacherProfilePage implements OnInit {
  public nbUsers = { lower: 2, upper: 4 };
  @Input() password: string;

  constructor(
    private router: Router,
    private userService: UserService,
    private state: StateService,
    private auth: AuthenticationService,
    private loading: LoadingService,
    private uploadService: UploadService
  ) {}

  public teacher: IUserInfo;

  ngOnInit() {
    this.state.user.subscribe((ob) => (this.teacher = ob));
  }

  async uploadProfilePic(files: FileList) {
    await this.loading.wrap(async () => await this._uploadProfilePic(files));
  }

  async updatePassword() {
    await this.loading.wrap(
      async () => await this.userService.updatePassword(this.password)
    );

    this.auth.logout();
    this.router.navigate(["teacher-login"]);
  }

  private async _uploadProfilePic(files: FileList) {
    const profilepic = await this.uploadService.upload(files.item(0));
    const res = await this.userService.updateUser({ profilepic });

    if (res) {
      await this.auth.update("TEACHER");
    }
  }
}
