import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { EditTeacherProfilePageRoutingModule } from "./edit-teacher-profile-routing.module";

import { EditTeacherProfilePage } from "./edit-teacher-profile.page";
import { ComponentsModule } from "@components/components.module";
import { TeacherHomePageRoutingModule } from "@pages/teacher-home/teacher-home-routing.module";
import { PipesModule } from "src/app/pipes/pipes.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    PipesModule,
    EditTeacherProfilePageRoutingModule
  ],
  declarations: [EditTeacherProfilePage]
})
export class EditTeacherProfilePageModule {}
