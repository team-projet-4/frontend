import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";
import { EditTeacherProfilePage } from "./edit-teacher-profile.page";
import { ToasterService } from "@services/frontend/toaster.service";
import { LoadingService } from "@services/frontend/loading.service";
import { Routes } from "@services/api/route.manager";
import { ComponentsModule } from "@components/components.module";
import { FormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";
import { PipesModule } from "src/app/pipes/pipes.module";

describe("EditTeacherProfilePage", () => {
  let component: EditTeacherProfilePage;
  let fixture: ComponentFixture<EditTeacherProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Routes,
          useFactory: (t: ToasterService, l: LoadingService) =>
            new Routes(t, l),
          deps: [ToasterService, LoadingService]
        }
      ],
      declarations: [EditTeacherProfilePage],
      imports: [
        IonicModule.forRoot(),
        ComponentsModule,
        PipesModule,
        FormsModule,
        RouterTestingModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(EditTeacherProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
