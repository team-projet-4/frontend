import { Component, OnInit } from "@angular/core";
import { MenuController } from "@ionic/angular";
import {
  IGetTeam,
  IUserInfo,
  MBTIAnswerFormPart,
  IUserInfoCourse,
} from "polyteam-shared";
import { StateService } from "@services/state/state.service";
import { Router } from "@angular/router";
import { LoadingService } from "@services/frontend/loading.service";
import { UploadService } from "@services/api/upload.service";
import { UserService } from "@services/api/user.service";
import { AuthenticationService } from "@services/api/authentication.service";
import { GetTeamService } from "@services/api/get-team.service";
import { UserState } from "@services/state/user.state";

@Component({
  selector: "app-user-home",
  templateUrl: "./user-home.page.html",
  styleUrls: ["./user-home.page.scss"],
})
export class UserHomePage implements OnInit {
  student: UserState;
  codes: Map<string, string>;
  public items: { course: IUserInfoCourse; expanded: boolean }[] = [];

  constructor(
    private state: StateService,
    private router: Router,
    private loading: LoadingService,
    private uploadService: UploadService,
    private userService: UserService,
    private getTeamService: GetTeamService,
    private auth: AuthenticationService
  ) {}

  async uploadProfilePic(files: FileList) {
    await this.loading.wrap(async () => await this._uploadProfilePic(files));
  }

  private async _uploadProfilePic(files: FileList) {
    const profilepic = await this.uploadService.upload(files.item(0));
    const res = await this.userService.updateUser({ profilepic });

    if (res) {
      await this.auth.update("USER");
    }
  }

  ngOnInit() {
    this.state.user.subscribe((ob) => (this.student = ob));
    this.student.courses.forEach((course) => {
      this.items.push({ course, expanded: false });
    });
  }

  gotoCourse(course: IUserInfoCourse) {
    this.router.navigate(["user-suggestion"], { queryParams: course });
  }

  gotoEditProfile() {
    this.router.navigate(["edit-profile"]);
  }

  gotoCreateTeamCode(course: IUserInfoCourse) {
    this.router.navigate(["create-team-code"], { queryParams: course });
  }

  expandItem(item): void {
    if (item.expanded) {
      item.expanded = false;
    } else {
      this.items.map((listItem) => {
        if (item === listItem) {
          listItem.expanded = !listItem.expanded;
        } else {
          listItem.expanded = false;
        }
        return listItem;
      });
    }
  }
}
