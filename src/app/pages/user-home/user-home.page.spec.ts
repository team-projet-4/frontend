import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { UserHomePage } from "./user-home.page";
import { ComponentsModule } from "@components/components.module";
import { RouterTestingModule } from "@angular/router/testing";
import { Routes } from "@services/api/route.manager";
import { ToasterService } from "@services/frontend/toaster.service";
import { LoadingService } from "@services/frontend/loading.service";
import { PipesModule } from "src/app/pipes/pipes.module";

describe("UserHomePage", () => {
  let component: UserHomePage;
  let fixture: ComponentFixture<UserHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Routes,
          useFactory: (t: ToasterService, l: LoadingService) =>
            new Routes(t, l),
          deps: [ToasterService, LoadingService]
        }
      ],
      declarations: [UserHomePage],
      imports: [
        IonicModule.forRoot(),
        ComponentsModule,
        RouterTestingModule,
        PipesModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(UserHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
