import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { UserHomePageRoutingModule } from "./user-home-routing.module";

import { UserHomePage } from "./user-home.page";
import { ComponentsModule } from "@components/components.module";
import { PipesModule } from "src/app/pipes/pipes.module";
import { UserService } from "@services/api/user.service";
import { UploadService } from "@services/api/upload.service";
import {ExpandableComponent} from "@components/expandable/expandable.component";
import {GetTeamService} from "@services/api/get-team.service";

@NgModule({
  imports: [
    ComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    UserHomePageRoutingModule
  ],
  providers: [UserService, UploadService, GetTeamService],
  declarations: [UserHomePage]
})
export class UserHomePageModule {}
