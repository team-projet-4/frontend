import { Component, OnInit } from "@angular/core";
import { CorePage } from "@core/core.page";
import { AuthenticationService } from "@services/api/authentication.service";
import { Router } from "@angular/router";
import { StrictNgForm } from "@core/core.types";
import { ILoginForm } from "polyteam-shared";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
})
export class LoginPage extends CorePage implements OnInit {
  constructor(private auth: AuthenticationService, private router: Router) {
    super();
  }

  public async ngOnInit() {}

  public async login(form: StrictNgForm<ILoginForm>) {
    const res = await this.auth.login(
      form.value.email,
      form.value.password,
      "USER"
    );

    if (res) {
      this.router.navigate(["user-home"]);
    } else {
    }
  }
}
