import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { CorePage } from "@core/core.page";
import { AuthenticationService } from "@services/api/authentication.service";
import { Router } from "@angular/router";
import { ToasterService } from "@services/frontend/toaster.service";
import { GroupService } from "@services/api/group.service";
import { LoadingService } from "@services/frontend/loading.service";

@Component({
  selector: "app-user-infos",
  templateUrl: "./user-infos.page.html",
  styleUrls: ["./user-infos.page.scss"],
})
export class UserInfosPage extends CorePage implements OnInit {
  public genderlist = [
    { key: "f", value: "Féminin" },
    { key: "m", value: "Masculin" },
  ];

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private router: Router,
    private toaster: ToasterService,
    private groupService: GroupService,
    private loading: LoadingService
  ) {
    super();
  }
  get gender() {
    return this.registrationForm.get("gender");
  }

  get orientation() {
    return this.registrationForm.get("orientation");
  }

  get phone() {
    return this.registrationForm.get("phone");
  }

  registrationForm: any;

  public errorMessages = {
    orientation: [
      {
        type: "required",
        message: "Veuillez remplir le champs de votre domaine d'études",
      },
    ],
    phone: [
      { type: "required", message: "Phone number is required" },
      { type: "pattern", message: "Please enter a valid phone number" },
    ],
  };

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      gender: [""],
      orientation: [""],
      phone: [""],
    });
  }

  public async submit() {
    await this.router.navigate(["user-home"]);
  }
}
