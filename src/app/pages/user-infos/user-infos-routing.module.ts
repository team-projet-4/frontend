import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { UserInfosPage } from "./user-infos.page";

const routes: Routes = [
  {
    path: "",
    component: UserInfosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserInfosPageRoutingModule {}
