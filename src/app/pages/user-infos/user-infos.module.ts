import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { UserInfosPageRoutingModule } from "./user-infos-routing.module";

import { UserInfosPage } from "./user-infos.page";
import {ComponentsModule} from "@components/components.module";
import {RouterModule} from "@angular/router";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: "",
                component: UserInfosPage
            }
        ]),
        UserInfosPageRoutingModule,
        ComponentsModule,
        ReactiveFormsModule
    ],
  declarations: [UserInfosPage]
})
export class UserInfosPageModule {}
