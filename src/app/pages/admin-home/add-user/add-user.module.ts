import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { AddUserPageRoutingModule } from "./add-user-routing.module";

import { AddUserPage } from "./add-user.page";
import { UserService } from "@services/api/user.service";

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, AddUserPageRoutingModule],
  providers: [UserService],
  declarations: [AddUserPage]
})
export class AddUserPageModule {}
