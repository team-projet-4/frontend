export interface NewUser {
  FirstName: string;
  LastName: string;
  emailAdresse: string;
  departement: string;
  Matricule: string;
  Cours: string[];
  admin: string;
}
