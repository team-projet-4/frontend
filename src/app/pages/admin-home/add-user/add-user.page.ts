import { Component, OnInit } from "@angular/core";
import { NewUser } from "./newUserInformation.model";
import { UserService } from "@services/api/user.service";
import { StrictNgForm } from "@core/core.types";
import { ICreateAccount, LoginType } from "polyteam-shared";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.page.html",
  styleUrls: ["./add-user.page.scss"],
})
export class AddUserPage implements OnInit {
  newUser: NewUser;

  constructor(private userService: UserService) {}

  ngOnInit() {}

  async submit(form: StrictNgForm<ICreateAccount & { type: LoginType }>) {
    let res = false;
    if (form.value.type === "ADMIN") {
      res = await this.userService.createAdmin(form.value);
    } else if (form.value.type === "TEACHER") {
      res = await this.userService.createTeacher(form.value);
    }
  }
}
