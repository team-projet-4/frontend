import { UserService } from "@services/api/user.service";
import { Component, OnInit } from "@angular/core";
import { GroupService } from "@services/api/group.service";
import { IUserPersonalInfo } from "polyteam-shared";

@Component({
  selector: "app-users-list",
  templateUrl: "./users-list.page.html",
  styleUrls: ["./users-list.page.scss"],
})
export class UsersListPage implements OnInit {
  users: IUserPersonalInfo[] = [];
  constructor(private userService: UserService) {}

  ngOnInit() {
    this.getAllUsers();
  }

  public async getAllUsers() {
    this.users = await this.userService.listAllUsers();
  }
}
