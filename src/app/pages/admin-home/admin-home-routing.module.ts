import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AdminHomePage } from "./admin-home.page";

const routes: Routes = [
  {
    path: "",
    component: AdminHomePage
  },
  {
    path: "add-user",
    loadChildren: () => import("./add-user/add-user.module").then( m => m.AddUserPageModule)
  },
  {
    path: "users-list",
    loadChildren: () => import("./users-list/users-list.module").then( m => m.UsersListPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminHomePageRoutingModule {}
