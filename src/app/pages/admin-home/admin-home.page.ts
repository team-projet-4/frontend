import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "@services/api/user.service";

@Component({
  selector: "app-admin-home",
  templateUrl: "./admin-home.page.html",
  styleUrls: ["./admin-home.page.scss"],
})
export class AdminHomePage implements OnInit {
  backupId: string;
  constructor(private router: Router, private userService: UserService) {}
  ngOnInit() {}

  onAddUserClick() {
    this.router.navigate(["admin-home/add-user"]);
  }
  onUsersListClick() {
    this.router.navigate(["admin-home/users-list"]);
  }

  onResetClick() {
    this.resetLists();
  }

  public async resetLists() {
    await this.userService.reset(this.backupId);
  }
}
