import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { EditProfilePageRoutingModule } from "./edit-profile-routing.module";
import { EditProfilePage } from "./edit-profile.page";
import { PipesModule } from "src/app/pipes/pipes.module";
import { ComponentsModule } from "@components/components.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    ComponentsModule,
    EditProfilePageRoutingModule
  ],
  declarations: [EditProfilePage]
})
export class EditProfilePageModule {}
