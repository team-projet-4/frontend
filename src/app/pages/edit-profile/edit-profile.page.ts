import { Component, OnInit, Input } from "@angular/core";
import { IUserInfo } from "polyteam-shared";
import { StateService } from "@services/state/state.service";
import { Router } from "@angular/router";
import { LoadingService } from "@services/frontend/loading.service";
import { UploadService } from "@services/api/upload.service";
import { UserService } from "@services/api/user.service";
import { AuthenticationService } from "@services/api/authentication.service";
@Component({
  selector: "app-edit-profile",
  templateUrl: "./edit-profile.page.html",
  styleUrls: ["./edit-profile.page.scss"],
})
export class EditProfilePage implements OnInit {
  student: IUserInfo;
  @Input() password: string;

  constructor(
    private state: StateService,
    private router: Router,
    private loading: LoadingService,
    private uploadService: UploadService,
    private userService: UserService,
    private auth: AuthenticationService
  ) {}

  async uploadProfilePic(files: FileList) {
    await this.loading.wrap(async () => await this._uploadProfilePic(files));
  }

  private async _uploadProfilePic(files: FileList) {
    const profilepic = await this.uploadService.upload(files.item(0));
    const res = await this.userService.updateUser({ profilepic });

    if (res) {
      await this.auth.update("USER");
    }
  }

  ngOnInit() {
    this.state.user.subscribe((ob) => (this.student = ob));
  }

  async updatePassword() {
    await this.loading.wrap(
      async () => await this.userService.updatePassword(this.password)
    );

    this.auth.logout();
    this.router.navigate(["login"]);
  }
}
