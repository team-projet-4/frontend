import { Component, OnInit } from "@angular/core";
import { CorePage } from "@core/core.page";
import { AuthenticationService } from "@services/api/authentication.service";
import { Router } from "@angular/router";
import { ToastController } from "@ionic/angular";
import { StrictNgForm } from "@core/core.types";
import { ILoginForm } from "polyteam-shared";

@Component({
  selector: "app-teacher-login",
  templateUrl: "./teacher-login.page.html",
  styleUrls: ["./teacher-login.page.scss"],
})
export class TeacherLoginPage extends CorePage implements OnInit {
  constructor(
    private auth: AuthenticationService,
    private toastController: ToastController,
    private router: Router
  ) {
    super();
  }

  public async ngOnInit() {}

  public async login(form: StrictNgForm<ILoginForm>) {
    const res = await this.auth.login(
      form.value.email,
      form.value.password,
      "TEACHER"
    );

    if (res) {
      this.router.navigate(["teacher-home"]);
    } else {
      await this.showToast();
    }
  }

  public async showToast() {
    const toast = await this.toastController.create({
      message: "Authentification Error",
      duration: 2000,
      animated: true,
      showCloseButton: true,
      closeButtonText: "OK",
      cssClass: "my-toast",
      position: "middle",
    });

    await toast.present();
  }
}
