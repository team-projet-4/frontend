import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { UserSuggestionPage } from "./user-suggestion.page";
import { ComponentsModule } from "@components/components.module";
import { RouterTestingModule } from "@angular/router/testing";
import { Routes } from "@services/api/route.manager";
import { ToasterService } from "@services/frontend/toaster.service";
import { LoadingService } from "@services/frontend/loading.service";
import { PipesModule } from "src/app/pipes/pipes.module";

describe("UserSuggestionPage", () => {
  let component: UserSuggestionPage;
  let fixture: ComponentFixture<UserSuggestionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Routes,
          useFactory: (t: ToasterService, l: LoadingService) =>
            new Routes(t, l),
          deps: [ToasterService, LoadingService]
        }
      ],
      declarations: [UserSuggestionPage],
      imports: [
        IonicModule.forRoot(),
        ComponentsModule,
        RouterTestingModule,
        PipesModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(UserSuggestionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
