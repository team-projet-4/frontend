import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { UserSuggestionPage } from "./user-suggestion.page";

const routes: Routes = [
  {
    path: " ",
    component: UserSuggestionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserSuggestionPageRoutingModule {}
