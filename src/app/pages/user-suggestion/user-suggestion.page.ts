import { Component, OnInit, ViewChild } from "@angular/core";
import { IonInfiniteScroll } from "@ionic/angular";
import { PopoverController } from "@ionic/angular";
import { RequestModalComponent } from "../../components/request-modal/request-modal.component";
import { GroupedObservable } from "rxjs";

import { CorePage } from "@core/core.page";
import { ActivatedRoute } from "@angular/router";
import { GroupService } from "@services/api/group.service";
import { IUserPublicInfo, LikeFactor, IUserInfoCourse } from "polyteam-shared";
import { IonSlides } from "@ionic/angular";
import { LoadingService } from "@services/frontend/loading.service";

@Component({
  selector: "app-user-suggestion",
  templateUrl: "./user-suggestion.page.html",
  styleUrls: ["./user-suggestion.page.scss"],
})
export class UserSuggestionPage extends CorePage implements OnInit {
  public teams: IUserPublicInfo[][] = [];
  private course: string;
  private infoCourse: IUserInfoCourse;

  @ViewChild(IonSlides, { static: false }) slides: IonSlides;

  constructor(
    private route: ActivatedRoute,
    private groupService: GroupService,
    private loading: LoadingService,
    public popover: PopoverController
  ) {
    super();
  }

  ngOnInit() {
    this.route.queryParams.subscribe(async (ob: IUserInfoCourse) => {
      this.infoCourse = ob;
      this.loading.wrap(async () => await this.init(ob.courseId));
    });
  }

  private async init(course: string) {
    this.teams = (await this.groupService.requestSuggestions(course)) || [];
    this.course = course;
    await this.slides.lockSwipes(true);
  }

  async likeUser(email: string, factor: number, i: number) {
    const res = await this.groupService.likeUser(
      this.course,
      email,
      factor as LikeFactor
    );

    if (res) {
      await this.createPopover();
    }

    this.teams.splice(i, 1);
  }

  private async createPopover() {
    const popoverElement = await this.popover.create({
      componentProps: { course: this.infoCourse },
      component: RequestModalComponent,
      showBackdrop: false,
    });

    await popoverElement.present();

    return new Promise(async (resolve) => {
      await popoverElement.onDidDismiss();
      resolve();
    });
  }
}
