import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { UserSuggestionPageRoutingModule } from "./user-suggestion-routing.module";

import { UserSuggestionPage } from "./user-suggestion.page";
import { RouterModule } from "@angular/router";
import { ComponentsModule } from "@components/components.module";
import { GroupService } from "@services/api/group.service";
import { PipesModule } from "src/app/pipes/pipes.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserSuggestionPageRoutingModule,
    RouterModule.forChild([
      {
        path: "",
        component: UserSuggestionPage
      }
    ]),
    PipesModule,
    ComponentsModule
  ],
  providers: [GroupService],
  declarations: [UserSuggestionPage]
})
export class UserSuggestionPageModule {}
