import { Component, OnInit } from "@angular/core";
import { CorePage } from "@core/core.page";
import { AuthenticationService } from "@services/api/authentication.service";
import { Router } from "@angular/router";
import { ToastController } from "@ionic/angular";
import { StrictNgForm } from "@core/core.types";
import { ILoginForm } from "polyteam-shared";

@Component({
  selector: "app-admin-login",
  templateUrl: "./admin-login.page.html",
  styleUrls: ["./admin-login.page.scss"],
})
export class AdminLoginPage extends CorePage implements OnInit {
  constructor(
    private auth: AuthenticationService,
    private toastController: ToastController,
    private router: Router
  ) {
    super();
  }

  public async ngOnInit() {}

  public async login(form: StrictNgForm<ILoginForm>) {
    const res = await this.auth.login(
      form.value.email,
      form.value.password,
      "ADMIN"
    );

    if (res) {
      await this.router.navigate(["admin-home"]);
    } else {
      await this.showToast();
    }
  }

  public async showToast() {
    const toast = await this.toastController.create({
      message: "Authentification Error",
      duration: 2000,
      animated: true,
      showCloseButton: true,
      closeButtonText: "OK",
      cssClass: "my-toast",
      position: "middle",
    });

    await toast.present();
  }
}
