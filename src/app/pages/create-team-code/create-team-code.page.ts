import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { GroupService } from "@services/api/group.service";
import { LoadingService } from "@services/frontend/loading.service";
import {
  ICourseTeam,
  ICourseTeamUser,
  IGetTeam,
  IJoinCode,
  IUserInfo,
  IUserPublicInfo,
  IUserInfoCourse,
} from "polyteam-shared";
import { GetTeamService } from "@services/api/get-team.service";
import { CorePage } from "@core/core.page";
import { StateService } from "@services/state/state.service";
import { ToasterService } from "@services/frontend/toaster.service";
import { AuthenticationService } from "@services/api/authentication.service";

@Component({
  selector: "app-create-team-code",
  templateUrl: "./create-team-code.page.html",
  styleUrls: ["./create-team-code.page.scss"],
})
export class CreateTeamCodePage extends CorePage implements OnInit {
  public courseID: IUserInfoCourse = {
    locked: false,
    courseId: "",
  };

  joinCode: string;
  student: IUserInfo;

  private userTeam: ICourseTeamUser = {
    email: " ",
    validated: true,
    fname: "",
    lname: "",
  };

  public course: IGetTeam = {
    courseId: " ",
  };

  public team: ICourseTeam = {
    users: [],
    ready: false,
  };

  public get validated() {
    const user = this.team.users.find((u) => u.email === this.student.email);
    return !user || user.validated;
  }

  public code: IJoinCode = {
    code: "",
  };

  public items = [
    { choice: 1, expanded: false },
    { choice: 2, expanded: false },
    { choice: 3, expanded: false },
  ];

  public teamUser: any;

  constructor(
    private state: StateService,
    private route: ActivatedRoute,
    private router: Router,
    private groupService: GroupService,
    private getTeamService: GetTeamService,
    private loading: LoadingService,
    private toaster: ToasterService,
    private auth: AuthenticationService
  ) {
    super();
  }

  ngOnInit() {
    this.state.user.subscribe((ob) => (this.student = ob));

    this.route.queryParams.subscribe(async (ob: IUserInfoCourse) => {
      this.code.code = "";
      this.loading.wrap(async () => await this.init(ob));
    });
  }

  private async init(course: IUserInfoCourse) {
    this.team = await this._getTeam(course);
    this.items.forEach((i) => (i.expanded = false));
  }

  public async _getTeam(course: IUserInfoCourse) {
    this.course.courseId = course.courseId;
    this.courseID = course;
    this.userTeam = {
      email: " ",
      validated: true,
      fname: "",
      lname: "",
    };
    const res = await this.getTeamService.getTeam(this.course);
    return res;
  }

  public async _getCode(courseID: string) {
    if (!this.student.codes[courseID]) {
      await this.getTeamService.requestTeamCode(this.course);
      await this.auth.update("USER");
    }

    this.code.code = this.student.codes[courseID];
  }

  public async _joinTeam() {
    this.code.code = this.joinCode;
    const res = await this.getTeamService.joinTeamUsingCode(this.code);
  }

  public async _validateTeam(course: IGetTeam) {
    await this.getTeamService.validateTeam(course);
    this.team = await this.getTeamService.getTeam(this.course);
  }

  async copyInputMessage(inputElement: any) {
    inputElement.select();
    document.execCommand("copy");
    inputElement.setSelectionRange(0, 0);
    await this.toaster.show("Votre code est copié");
  }

  gotoCourse(course: IUserInfoCourse) {
    this.router.navigate(["user-suggestion"], { queryParams: course });
  }

  async expandItem(item: any) {
    if (item.expanded) {
      item.expanded = false;
    } else {
      if (item === this.items[2]) {
        await this._getCode(this.courseID.courseId);
      }
      this.items.forEach((listItem) => {
        if (item === listItem) {
          listItem.expanded = !listItem.expanded;
        } else {
          listItem.expanded = false;
        }
        return listItem;
      });
    }
  }
}
