import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { CreateTeamCodePageRoutingModule } from "./create-team-code-routing.module";

import { CreateTeamCodePage } from "./create-team-code.page";
import { PipesModule } from "src/app/pipes/pipes.module";
import { UserService } from "@services/api/user.service";
import { ComponentsModule } from "@components/components.module";
import { GetTeamService } from "@services/api/get-team.service";
import { RouterModule } from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: "",
        component: CreateTeamCodePage,
      },
    ]),
    CreateTeamCodePageRoutingModule,
    PipesModule,
    ComponentsModule,
  ],
  providers: [UserService, GetTeamService],
  declarations: [CreateTeamCodePage],
})
export class CreateTeamCodePageModule {}
