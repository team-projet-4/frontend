import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CreateTeamCodePage } from "./create-team-code.page";

const routes: Routes = [
  {
    path: " ",
    component: CreateTeamCodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateTeamCodePageRoutingModule {}
