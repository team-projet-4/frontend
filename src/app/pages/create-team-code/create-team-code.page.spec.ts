import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { CreateTeamCodePage } from "./create-team-code.page";
import { Routes } from "@services/api/route.manager";
import { ToasterService } from "@services/frontend/toaster.service";
import { LoadingService } from "@services/frontend/loading.service";
import { ComponentsModule } from "@components/components.module";
import { PipesModule } from "src/app/pipes/pipes.module";
import { FormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";

describe("CreateTeamCodePage", () => {
  let component: CreateTeamCodePage;
  let fixture: ComponentFixture<CreateTeamCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Routes,
          useFactory: (t: ToasterService, l: LoadingService) =>
            new Routes(t, l),
          deps: [ToasterService, LoadingService],
        },
      ],
      declarations: [CreateTeamCodePage],
      imports: [
        IonicModule.forRoot(),
        ComponentsModule,
        PipesModule,
        FormsModule,
        RouterTestingModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CreateTeamCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
