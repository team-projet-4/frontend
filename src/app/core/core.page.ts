import { DeviceUtils } from "@utils/device.utils";

export class CorePage {
  public get isMobile() {
    return DeviceUtils.isMobile;
  }
}
