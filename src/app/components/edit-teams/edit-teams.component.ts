import { CorePage } from "./../../core/core.page";
import { Team, ITeamMember } from "./../display-teams/team.model";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { GroupService } from "@services/api/group.service";
import { ICourseTeam } from "polyteam-shared";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from "@angular/cdk/drag-drop";
import { DragulaService } from "ng2-dragula";
import { ToastController } from "@ionic/angular";
import { AppModule } from "../../app.module";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

@Component({
  selector: "app-edit-teams",
  templateUrl: "./edit-teams.component.html",
  styleUrls: ["./edit-teams.component.scss"],
})
export class EditTeamsComponent extends CorePage implements OnInit {
  constructor(
    private acivatedRoute: ActivatedRoute,
    private groupService: GroupService,
    private dragulaService: DragulaService,
    private toastController: ToastController
  ) {
    super();
  }

  // TODO : Add a shared service between Display and edit
  cours: string;
  teams: ICourseTeam[];
  loadedClass: Team[];

  public async getTeamsList(courseId: string) {
    return this.groupService.getTeams(courseId);
  }

  public async updateTeamsList() {
    return this.groupService.updateTeams(
      this.cours,
      this.convertBack(this.loadedClass)
    );
  }

  async ngOnInit() {
    this.acivatedRoute.paramMap.subscribe((paramMap) => {
      if (!paramMap.has("courseId")) {
        // redirect
        return;
      }
      const courseId = paramMap.get("courseId");
      this.cours = courseId;
    });
    this.teams = await this.getTeamsList(this.cours);
    this.loadedClass = this.convertToTeam(this.teams);
  }

  public convertToTeam(teams: ICourseTeam[]): Team[] {
    return teams.map((t, i) => ({
      id: `Team ${i}`,
      members: t.users,
      full: t.ready,
    }));
  }

  public convertBack(teams: Team[]): ICourseTeam[] {
    return teams.map((t) => ({
      users: t.members.map(({ email, fname, lname }) => ({
        email,
        fname,
        lname,
        validated: true,
      })),
      ready: t.full,
    }));
  }

  public addNewTeam() {
    this.loadedClass.push(
      new Team(`Team ${this.loadedClass.length.toString()}`, [], false)
    );
  }

  public addNewTeams(id: string, members: ITeamMember[], full: boolean) {
    this.loadedClass.push(new Team("", members, false));
  }

  public dissolveTeam(teamID: string) {
    const idx = this.loadedClass.findIndex((e) => e.id === teamID);
    if (idx === -1) {
      return;
    }

    this.loadedClass[idx].members.forEach((e) => {
      this.addNewTeams("", [e], false);
    });

    this.loadedClass.splice(idx, 1);
    this.loadedClass.forEach((t, i) => (t.id = `Team ${i.toString()}`));
  }

  // Source: https://github.com/valor-software/ng2-dragula#readme
}
