import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { EditTeamsComponent } from "./edit-teams.component";

describe("EditTeamsComponent", () => {
  let component: EditTeamsComponent;
  let fixture: ComponentFixture<EditTeamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditTeamsComponent],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(EditTeamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  // it("should create", () => {
  //   expect(component).toBeTruthy();
  // });
});
