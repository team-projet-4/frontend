import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule, NavParams } from "@ionic/angular";

import { RequestModalComponent } from "./request-modal.component";
import { RouterTestingModule } from "@angular/router/testing";

class NavParamsMock {
  static returnParam = null;
  static setParams(value: any) {
    NavParamsMock.returnParam = value;
  }

  public get(key: any): any {
    if (NavParamsMock.returnParam) {
      return NavParamsMock.returnParam;
    }
    return "default";
  }
}

describe("RequestModalComponent", () => {
  let component: RequestModalComponent;
  let fixture: ComponentFixture<RequestModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RequestModalComponent],
      imports: [IonicModule.forRoot(), RouterTestingModule],
      providers: [{ provide: NavParams, useClass: NavParamsMock }],
    }).compileComponents();

    fixture = TestBed.createComponent(RequestModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
