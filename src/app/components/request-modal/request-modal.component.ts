import { Component, OnInit } from "@angular/core";
import { PopoverController, NavParams } from "@ionic/angular";
import { CorePage } from "@core/core.page";
import { IUserInfoCourse } from "polyteam-shared";
import { Router } from "@angular/router";

@Component({
  selector: "app-request-modal",
  templateUrl: "./request-modal.component.html",
  styleUrls: ["./request-modal.component.scss"],
})
export class RequestModalComponent extends CorePage implements OnInit {
  private course: IUserInfoCourse;

  constructor(
    private popover: PopoverController,
    private navParams: NavParams,
    private router: Router
  ) {
    super();
    this.course = navParams.get("course");
  }

  ngOnInit() {}

  closePopover() {
    this.popover.dismiss();
  }

  gotoTeam() {
    this.router.navigate(["create-team-code"], { queryParams: this.course });
  }
}
