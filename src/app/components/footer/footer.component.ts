import { Component, OnInit } from "@angular/core";
import { CorePage } from "@core/core.page";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"]
})
export class FooterComponent extends CorePage implements OnInit {
  constructor() {
    super();
  }

  ngOnInit() {}
}
