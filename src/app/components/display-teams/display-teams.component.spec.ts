import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { DisplayTeamsComponent } from "./display-teams.component";

describe("DisplayTeamsComponent", () => {
  let component: DisplayTeamsComponent;
  let fixture: ComponentFixture<DisplayTeamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DisplayTeamsComponent],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(DisplayTeamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  // it("should create", () => {
  //   expect(component).toBeTruthy();
  // });
});
