import { Component, OnInit } from "@angular/core";
import { Team } from "./team.model";
import { ActivatedRoute } from "@angular/router";
import { GroupService } from "../../services/api/group.service";
import { ICourseTeam } from "polyteam-shared";

@Component({
  selector: "app-display-teams",
  templateUrl: "./display-teams.component.html",
  styleUrls: ["./display-teams.component.scss"],
})
export class DisplayTeamsComponent implements OnInit {
  // using a made up team for now to test UI
  cours: string;
  teams: ICourseTeam[];
  loadedClass: Team[];
  private liste: any[] = [];
  headerList: string[];
  constructor(
    private acivatedRoute: ActivatedRoute,
    private groupService: GroupService
  ) {}

  public async getTeamsList(courseId: string) {
    return this.groupService.getTeams(courseId);
  }

  async ngOnInit() {
    this.acivatedRoute.paramMap.subscribe((paramMap) => {
      if (!paramMap.has("courseId")) {
        // redirect
        return;
      }
      const courseId = paramMap.get("courseId");
      this.cours = courseId;
    });
    this.teams = await this.getTeamsList(this.cours);
    this.loadedClass = this.convertToTeam(this.teams);
  }

  public convertToTeam(teams: ICourseTeam[]): Team[] {
    return teams.map((t, i) => ({
      id: `Team ${i}`,
      members: t.users,
      full: t.ready,
    }));
  }

  public exportTeams(teams: ICourseTeam[]) {
    this.headerList = ["team", "fname", "lname", "email"];
    let index = 1;
    this.teams.map((team) => {
      team.users.map((member) => {
        const temp = {
          team: index,
          fname: member.fname,
          lname: member.lname,
          email: member.email,
        };
        this.liste.push(temp);
      });
      index++;
    });
    const csv = this.ConvertToCSV(this.liste, this.headerList);
    this.exportCSV(csv);
  }

  ConvertToCSV(objArray: any[], headerList: string[]) {
    const array =
      typeof objArray !== "object" ? JSON.parse(objArray) : objArray;
    let str = "";
    let row = "";
    // tslint:disable-next-line:forin
    for (const index in headerList) {
      row += headerList[index] + ",";
    }
    row = row.slice(0, -1);
    str += row + "\r\n";
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < array.length; i++) {
      let line = "";
      // tslint:disable-next-line:forin
      for (const index in headerList) {
        const head = headerList[index];
        if (line === "") {
          line += array[i][head];
        } else {
          line += "," + array[i][head];
        }
      }
      str += line + "\r\n";
    }
    return str;
  }

  public exportCSV(csvFile: any) {
    const blob = new Blob([csvFile], { type: "text/csv;charset=utf-8;" });

    const link = document.createElement("a");
    if (link.download !== undefined) {
      const url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", "equipes.csv");
      link.style.visibility = "hidden";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
}
