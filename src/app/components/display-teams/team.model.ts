export interface ITeamMember {
  fname: string;
  lname: string;
  email: string;
}

export class Team {
  public id: string;
  public members: ITeamMember[];
  public full: boolean;

  constructor(id: string, members: ITeamMember[], full: boolean) {
    this.id = id;
    this.members = members;
    this.full = full;
  }
}
