import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { RouterModule } from "@angular/router";
import { RequestModalComponent } from "./request-modal/request-modal.component";
import { AuthenticationService } from "@services/api/authentication.service";
import { TeamsHeaderComponent } from "./teams-header/teams-header.component";
import { ExpandableComponent } from "@components/expandable/expandable.component";

const COMPONENTS = [
  HeaderComponent,
  FooterComponent,
  TeamsHeaderComponent,
  RequestModalComponent,
  ExpandableComponent,
];

@NgModule({
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [AuthenticationService],
  entryComponents: COMPONENTS,
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule,
  ],
})
export class ComponentsModule {}
