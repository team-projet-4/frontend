import { HeaderComponent } from "./../header/header.component";
import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { CorePage } from "@core/core.page";
import { Router } from "@angular/router";
import { AuthenticationService } from "@services/api/authentication.service";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

@Component({
  selector: "app-teams-header",
  templateUrl: "./teams-header.component.html",
  styleUrls: ["./teams-header.component.scss"],
})
export class TeamsHeaderComponent extends CorePage implements OnInit {
  constructor(private router: Router, private auth: AuthenticationService) {
    super();
  }

  @Output() featureSelected = new EventEmitter<string>();

  logout() {
    this.auth.logout();
    this.router.navigate(["home"]);
  }

  onSelect(feature: string) {
    this.featureSelected.emit(feature);
  }
  ngOnInit() {}
}
