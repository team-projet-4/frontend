import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { TeamsHeaderComponent } from "./teams-header.component";

describe("TeamsHeaderComponent", () => {
  let component: TeamsHeaderComponent;
  let fixture: ComponentFixture<TeamsHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TeamsHeaderComponent],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(TeamsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  // it("should create", () => {
  //   expect(component).toBeTruthy();
  // });
});
