import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { HeaderComponent } from "./header.component";
import { RouterTestingModule } from "@angular/router/testing";
import { Routes } from "@services/api/route.manager";
import { ToasterService } from "@services/frontend/toaster.service";
import { LoadingService } from "@services/frontend/loading.service";

describe("HeaderComponent", () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Routes,
          useFactory: (t: ToasterService, l: LoadingService) =>
            new Routes(t, l),
          deps: [ToasterService, LoadingService]
        }
      ],
      declarations: [HeaderComponent],
      imports: [IonicModule.forRoot(), RouterTestingModule]
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
