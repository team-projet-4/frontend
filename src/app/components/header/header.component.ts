import { Component, OnInit } from "@angular/core";
import { CorePage } from "@core/core.page";
import { Router } from "@angular/router";
import { AuthenticationService } from "@services/api/authentication.service";
import { StateService } from "@services/state/state.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent extends CorePage implements OnInit {
  constructor(
    private router: Router,
    private auth: AuthenticationService,
    private state: StateService
  ) {
    super();
  }

  gotoHome() {
    if (!this.state.user || !this.state.user.value.logintype) {
      this.router.navigate(["home"]);
    }

    switch (this.state.user.value.logintype) {
      case "ADMIN":
        this.router.navigate(["admin-home"]);
        break;

      case "TEACHER":
        this.router.navigate(["teacher-home"]);
        break;

      case "USER":
        this.router.navigate(["user-home"]);
        break;

      default:
        this.router.navigate(["home"]);
    }
  }

  logout() {
    this.auth.logout();
    this.router.navigate(["home"]);
  }

  ngOnInit() {}
}
