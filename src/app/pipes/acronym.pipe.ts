import { Pipe, PipeTransform } from "@angular/core";
import { IUserInfoCourse } from "polyteam-shared";

@Pipe({ name: "acronym" })
export class AcronymPipe implements PipeTransform {
  transform(value: IUserInfoCourse): string {
    const [acronym, section, type] = value.courseId.split("_");
    return `${acronym} (${section}) ${type === "l" ? "Lab." : "Th."}`;
  }
}
