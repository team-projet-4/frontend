import { Pipe, PipeTransform } from "@angular/core";
import { environment } from "src/environments/environment";

@Pipe({ name: "image" })
export class ImagePipe implements PipeTransform {
  transform(value?: string): string {
    if (!value) {
      return "assets/images/no-profile-picture.jfif";
    }
    return environment.BACKEND_URL + "/" + value;
  }
}
