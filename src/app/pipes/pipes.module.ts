import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AcronymPipe } from "./acronym.pipe";
import { PersonalityPipe } from "./personality.pipe";
import { ImagePipe } from "./image.pipe";

const PIPES = [AcronymPipe, PersonalityPipe, ImagePipe];

@NgModule({
  declarations: PIPES,
  imports: [CommonModule],
  exports: PIPES
})
export class PipesModule {}
