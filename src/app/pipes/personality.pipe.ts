import { Pipe, PipeTransform } from "@angular/core";
import { PersonalityType } from "polyteam-shared";

@Pipe({ name: "personality" })
export class PersonalityPipe implements PipeTransform {
  transform(value?: PersonalityType): string {
    if (!value) {
      return "Inconnue";
    }

    return value.join("");
  }
}
