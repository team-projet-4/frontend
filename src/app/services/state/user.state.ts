import {
  IUserInfo,
  LoginType,
  PersonalityType,
  IUserInfoCourse,
} from "polyteam-shared";
import { Map } from "@core/core.types";

export class UserState implements IUserInfo {
  email: string;
  courses: IUserInfoCourse[];
  jwt: string;
  fname: string;
  lname: string;
  profilepic: string;
  logintype: LoginType;
  personality: PersonalityType;
  codes: Map<string>;

  public constructor(info?: IUserInfo) {
    this.email = info ? info.email : "";
    this.courses = info ? info.courses : [];
    this.jwt = info ? info.jwt : "";
    this.fname = info ? info.fname : "";
    this.lname = info ? info.lname : "";
    this.profilepic = info ? info.profilepic : "";
    this.logintype = info ? info.logintype : undefined;
    this.personality = info ? info.personality : undefined;
    this.codes = info ? info.codes : {};
  }
}
