import { Injectable } from "@angular/core";
import { UserState } from "./user.state";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class StateService {
  public user?: BehaviorSubject<UserState> = new BehaviorSubject(
    new UserState()
  );

  public clear() {
    this.user.next(new UserState());
  }
}
