import { TestBed } from "@angular/core/testing";

import { GroupService } from "./group.service";
import { Routes } from "./route.manager";
import { ToasterService } from "@services/frontend/toaster.service";
import { LoadingService } from "@services/frontend/loading.service";

describe("GroupService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Routes,
          useFactory: (t: ToasterService, l: LoadingService) =>
            new Routes(t, l),
          deps: [ToasterService, LoadingService]
        }
      ]
    })
  );

  it("should be created", () => {
    const service: GroupService = TestBed.get(GroupService);
    expect(service).toBeTruthy();
  });
});
