import { Injectable } from "@angular/core";
import { Routes } from "./route.manager";
import { StateService } from "@services/state/state.service";
import {
  LikeFactor,
  MBTIAnswerForm,
  MBTIForm,
  ICourseTeam,
} from "polyteam-shared";

@Injectable({
  providedIn: "root",
})
export class GroupService {
  constructor(private state: StateService, private R: Routes) {}

  public async requestSuggestions(acronym: string) {
    const res = await this.R.suggestion.emit({ acronym }, false);
    return res.data.data;
  }

  public async requestMBTIForm() {
    const r = await this.R.mbtiForm.emit({}, false);
    const form: MBTIForm = r.data.data;
    return form;
  }

  public async requestMBTIPersonality(MBTIAnswers: MBTIAnswerForm) {
    const res = await this.R.mbtiProcess.emit(MBTIAnswers, false);

    if (res.status !== 200) {
      return undefined;
    }

    return res.data.data;
  }

  public async likeUser(
    courseId: string,
    userEmail: string,
    likeFactor: LikeFactor
  ) {
    const form = { courseId, userEmail, likeFactor };
    const res = await this.R.likeUser.emit(form, false);
    return res.status !== 200 ? false : res.data.data.matched;
  }

  public async getTeams(courseId: string) {
    const res = await this.R.getTeams.emit({ courseId });
    return res.status === 200 ? res.data.data : [];
  }

  public async updateTeams(courseId: string, teams: ICourseTeam[]) {
    await this.R.updateTeams.emit({ courseId, teams });
  }

  public async createTeams(courseId: string) {
    await this.R.createTeams.emit({ courseId });
  }

}
