import {
  RouteVerifier,
  RouteMethod,
  IServerResponse,
  RouteKey,
  ROUTES as R,
} from "polyteam-shared";
import { ApiServiceType as Api, ApiResponse, ApiService } from "./api.service";
import { StrictMap } from "@core/core.types";
import { ToasterService } from "@services/frontend/toaster.service";
import { LoadingService } from "@services/frontend/loading.service";

class FrontendEmitter<I, O> implements RouteVerifier<I, O> {
  url: string;
  method: RouteMethod;
  in: (o: any) => o is I;
  out: (o: any) => o is any;

  public constructor(
    private toaster: ToasterService,
    private loading: LoadingService,
    v: RouteVerifier<I, O>
  ) {
    this.url = v.url;
    this.method = v.method;
    this.in = v.in;
    this.out = this.transformVerifier(v.out);
  }

  public async emit(i: I, success = true, bg = false): Promise<ApiResponse<O>> {
    return bg
      ? await this._emit(i, false)
      : await this.loading.wrap(async () => await this._emit(i, success));
  }

  private async _emit(input: I, successToast = true): Promise<ApiResponse<O>> {
    const res = await this.submit(input);

    if (res.status >= 300) {
      await this.toaster.error(res.data.msg || "L'opération a échoué.");
    } else {
      if (successToast) {
        await this.toaster.show(res.data.msg || "L'opération a réussi.");
      }
    }

    return res;
  }

  private async submit(input: I): Promise<ApiResponse<O>> {
    const res = await this._submit(ApiService, input);

    if (res.status >= 300) {
      return res;
    }

    if (this.out(res)) {
      return res;
    }

    return {
      status: 500,
      data: {
        msg: "Une erreur est survenu lors du traitement de votre requete.",
      },
    };
  }

  private async _submit(api: Api, input: I): Promise<ApiResponse<O>> {
    if (!this.in(input)) {
      return {
        status: 400,
        data: { msg: "Les paramètres du formulaire sont invalide." },
      };
    }

    return await api[this.method](this.url, input);
  }

  private transformVerifier(
    v: (o: any) => o is IServerResponse<O>
  ): (o: any) => o is ApiResponse<O> {
    return ((o: any) => {
      if (!o.data) {
        o.data = {};
      }
      o.data.code = o.status;
      return v(o.data);
    }) as (o: any) => o is ApiResponse<O>;
  }
}

export class Routes implements StrictMap<RouteKey, FrontendEmitter<any, any>> {
  public constructor(
    t: ToasterService,
    l: LoadingService,
    public login = new FrontendEmitter(t, l, R.login),
    public token = new FrontendEmitter(t, l, R.token),
    public suggestion = new FrontendEmitter(t, l, R.suggestion),
    public mbtiForm = new FrontendEmitter(t, l, R.mbtiForm),
    public mbtiProcess = new FrontendEmitter(t, l, R.mbtiProcess),
    public admin = new FrontendEmitter(t, l, R.admin),
    public teacher = new FrontendEmitter(t, l, R.teacher),
    public teacherList = new FrontendEmitter(t, l, R.teacherList),
    public upload = new FrontendEmitter(t, l, R.upload),
    public updateUser = new FrontendEmitter(t, l, R.updateUser),
    public likeUser = new FrontendEmitter(t, l, R.likeUser),
    public getTeam = new FrontendEmitter(t, l, R.getTeam),
    public getTeams = new FrontendEmitter(t, l, R.getTeams),
    public updateTeams = new FrontendEmitter(t, l, R.updateTeams),
    public createCode = new FrontendEmitter(t, l, R.createCode),
    public joinCode = new FrontendEmitter(t, l, R.joinCode),
    public validateTeam = new FrontendEmitter(t, l, R.validateTeam),
    public createTeams = new FrontendEmitter(t, l, R.createTeams),
    public listAllUsers = new FrontendEmitter(t, l, R.listAllUsers),
    public reset = new FrontendEmitter(t, l, R.reset)
  ) {}
}
