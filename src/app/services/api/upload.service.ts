import { Injectable } from "@angular/core";
import { Routes } from "./route.manager";

@Injectable({
  providedIn: "root"
})
export class UploadService {
  constructor(private R: Routes) {}

  public async upload(file: File) {
    const form = new FormData();
    form.append("upload", file);

    const res = await this.R.upload.emit(form);

    if (res.status >= 300) {
      return undefined;
    }

    return res.data.data;
  }
}
