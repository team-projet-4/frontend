import { TestBed } from "@angular/core/testing";

import { GetTeamService } from "./get-team.service";
import { Routes } from "./route.manager";
import { ToasterService } from "@services/frontend/toaster.service";
import { LoadingService } from "@services/frontend/loading.service";

describe("GetTeamService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Routes,
          useFactory: (t: ToasterService, l: LoadingService) =>
            new Routes(t, l),
          deps: [ToasterService, LoadingService],
        },
      ],
    })
  );

  it("should be created", () => {
    const service: GetTeamService = TestBed.get(GetTeamService);
    expect(service).toBeTruthy();
  });
});
