import { Injectable } from "@angular/core";
import { LoginType, IUserInfo } from "polyteam-shared";
import { StateService } from "@services/state/state.service";
import { UserState } from "@services/state/user.state";
import { Routes } from "./route.manager";

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  private interval: NodeJS.Timer;

  constructor(private state: StateService, private R: Routes) {}

  public async update(loginType: LoginType) {
    return await this.pull(loginType, false);
  }

  public async pull(loginType: LoginType, successToast = true, trigger = true) {
    if (!localStorage.getItem("jwt") || localStorage.getItem("jwt") === "") {
      return false;
    }

    const res = await this.R.token.emit({ loginType }, successToast, !trigger);

    if (res.status !== 200) {
      return false;
    }

    this.restoreState(res.data.data);

    if (trigger) {
      this.triggerUpdate(loginType);
    }

    return true;
  }

  public async login(email: string, password: string, loginType: LoginType) {
    if (this.interval) {
      clearInterval(this.interval);
    }

    const res = await this.R.login.emit({ email, password, loginType });

    if (res.status !== 200) {
      return false;
    }

    this.restoreState(res.data.data);
    this.triggerUpdate(loginType);

    return true;
  }

  public logout() {
    localStorage.clear();
    this.state.clear();
    clearInterval(this.interval);
  }

  private triggerUpdate(loginType: LoginType) {
    if (this.interval) {
      clearInterval(this.interval);
    }

    // EVERY 5s
    this.interval = setInterval(
      () => this.pull(this.state.user.value.logintype, false, false),
      5000
    );
  }

  private restoreState(userState: IUserInfo) {
    localStorage.setItem("jwt", userState.jwt);
    this.state.user.next(new UserState(userState));
  }
}
