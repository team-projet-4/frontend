import { Injectable } from "@angular/core";
import { IGetTeam, IJoinCode } from "polyteam-shared";
import { Routes } from "@services/api/route.manager";

@Injectable({
  providedIn: "root",
})
export class GetTeamService {
  constructor(private R: Routes) {}

  public async requestTeamCode(courseID: IGetTeam) {
    const res = await this.R.createCode.emit(courseID);
    return res.data.data;
  }

  public async joinTeamUsingCode(joinCode: IJoinCode) {
    const res = await this.R.joinCode.emit(joinCode);
    return res.data.data;
  }

  public async getTeam(userTeam: IGetTeam) {
    const res = await this.R.getTeam.emit(userTeam);
    return res.data.data;
  }

  public async validateTeam(userTeam: IGetTeam) {
    await this.R.validateTeam.emit(userTeam);
  }
}
