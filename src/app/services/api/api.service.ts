import axios from "axios";
import { environment } from "../../../environments/environment";

export interface ApiContent<T> {
  msg?: string;
  data?: T;
}

export interface ApiResponse<T> {
  data: ApiContent<T>;
  status: number;
}

function intercept(_: any, __: string, descriptor: PropertyDescriptor) {
  const original = descriptor.value;

  descriptor.value = async function(...args: any[]) {
    try {
      const res = await original.apply(this, args);
      return res;
    } catch (e) {
      if (e.response) {
        return e.response;
      }

      if (e.request) {
        return {
          status: 500,
          data: {
            msg: "Une erreur est survenu lors du traitement de votre requete."
          }
        };
      }
    }
  };
}

class ApiServiceDefinition {
  private static jwt: string;
  private static readonly BASE_URL = environment.BACKEND_URL;

  @intercept
  public static async get<I, O>(url: string, _: I): Promise<ApiResponse<O>> {
    return await axios.get<ApiContent<O>>(this.BASE_URL + url, this.headers);
  }

  @intercept
  public static async post<I, O>(
    url: string,
    body: I
  ): Promise<ApiResponse<O>> {
    return await axios.post<ApiContent<O>>(
      this.BASE_URL + url,
      body,
      this.headers
    );
  }

  @intercept
  public static async put<I, O>(url: string, body: I): Promise<ApiResponse<O>> {
    return await axios.put<ApiContent<O>>(
      this.BASE_URL + url,
      body,
      this.headers
    );
  }

  @intercept
  public static async delete<I, O>(url: string, _: I): Promise<ApiResponse<O>> {
    return await axios.delete<ApiContent<O>>(this.BASE_URL + url, this.headers);
  }

  private static get headers() {
    this.jwt = localStorage.getItem("jwt");

    const headers = {
      authorization: this.jwt
    };

    return { headers };
  }
}

export type ApiServiceType = typeof ApiServiceDefinition;
export const ApiService: ApiServiceType = ApiServiceDefinition;
