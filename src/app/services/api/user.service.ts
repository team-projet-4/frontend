import { Injectable } from "@angular/core";
import { Routes } from "./route.manager";
import { StateService } from "@services/state/state.service";
import {
  ICreateAccount,
  ICreateUserGroup,
  IUpdateAccount,
} from "polyteam-shared";

@Injectable({
  providedIn: "root",
})
export class UserService {
  constructor(private state: StateService, private R: Routes) {}

  public async createAdmin(form: ICreateAccount) {
    const res = await this.R.admin.emit(form);

    return res.status === 200;
  }

  public async createTeacher(form: ICreateAccount) {
    const res = await this.R.teacher.emit(form);

    return res.status === 200;
  }

  public async uploadList(form: ICreateUserGroup) {
    const res = await this.R.teacherList.emit(form);

    return res.status === 200;
  }

  public async updateUser(form: IUpdateAccount) {
    const res = await this.R.updateUser.emit(form);
    return res.status === 200;
  }

  public async updatePassword(password: string) {
    const res = await this.R.updateUser.emit({ password });
    return res.status === 200;
  }

  public async listAllUsers() {
    const res = await this.R.listAllUsers.emit({});
    return res.status === 200 ? res.data.data : [];
  }

  public async reset(key: string) {
    await this.R.reset.emit({ key });
  }
}
