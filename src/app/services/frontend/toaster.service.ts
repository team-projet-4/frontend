import { Injectable } from "@angular/core";
import { ToastController } from "@ionic/angular";
import { DeviceUtils } from "@utils/device.utils";

@Injectable({
  providedIn: "root",
})
export class ToasterService {
  constructor(private toastController: ToastController) {}

  public async show(msg: string) {
    await this.showToast(msg, "primary-toast");
  }

  public async error(msg: string) {
    await this.showToast(msg, "error-toast");
  }

  private async showToast(message: string, cssClass: string) {
    if (!message || message === "") {
      return;
    }

    const toast = await this.toastController.create({
      message,
      duration: 2000,
      animated: true,
      showCloseButton: true,
      closeButtonText: "OK",
      cssClass,
      position: DeviceUtils.isMobile ? "top" : "middle",
    });

    await toast.present();
  }
}
