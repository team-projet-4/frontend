import { Injectable } from "@angular/core";
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { StateService } from "@services/state/state.service";
import { ToasterService } from "@services/frontend/toaster.service";
import { LoginType } from "polyteam-shared";
import { AuthenticationService } from "@services/api/authentication.service";

class Guard implements CanActivate {
  constructor(
    protected auth: AuthenticationService,
    protected router: Router,
    protected state: StateService,
    protected toaster: ToasterService,
    protected loginType: LoginType,
    protected loginRoute: string,
    protected title: string
  ) {}

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.check) {
      return true;
    }

    const res = await this.auth.pull(this.loginType);

    if (res && this.check) {
      return true;
    }

    await this.toaster.error(
      `Vous devez être connecté en tant qu'${this.title} pour pouvoir accèder à cette fonctionalité.`
    );
    await this.router.navigate([this.loginRoute]);
  }

  private get check() {
    return (
      this.state.user.value &&
      this.state.user.value.logintype === this.loginType
    );
  }
}

@Injectable()
export class AdminGuard extends Guard {
  constructor(
    protected auth: AuthenticationService,
    protected router: Router,
    protected state: StateService,
    protected toaster: ToasterService
  ) {
    super(
      auth,
      router,
      state,
      toaster,
      "ADMIN",
      "admin-login",
      "administrateur"
    );
  }
}

@Injectable()
export class TeacherGuard extends Guard {
  constructor(
    protected auth: AuthenticationService,
    protected router: Router,
    protected state: StateService,
    protected toaster: ToasterService
  ) {
    super(
      auth,
      router,
      state,
      toaster,
      "TEACHER",
      "teacher-login",
      "professeur"
    );
  }
}

@Injectable()
export class UserGuard extends Guard {
  constructor(
    protected auth: AuthenticationService,
    protected router: Router,
    protected state: StateService,
    protected toaster: ToasterService
  ) {
    super(auth, router, state, toaster, "USER", "login", "étudiant");
  }
}

@Injectable()
export class UserGuardMBTI implements CanActivate {
  constructor(
    protected userGuard: UserGuard,
    protected router: Router,
    protected state: StateService,
    protected toaster: ToasterService
  ) {}

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!(await this.userGuard.canActivate(route, state))) {
      return false;
    }

    if (this.state.user.value && this.state.user.value.personality) {
      return true;
    }

    await this.toaster.show(
      `Vous devez compléter le test de personalité MBTI.`
    );
    await this.router.navigate(["test-mbti"]);
  }
}
