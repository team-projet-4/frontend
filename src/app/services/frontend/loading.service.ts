import { Injectable } from "@angular/core";
import { LoadingController } from "@ionic/angular";

@Injectable({
  providedIn: "root"
})
export class LoadingService {
  private locks = [];
  private loading: HTMLIonLoadingElement;

  constructor(private loadingController: LoadingController) {}

  public async wrap<T>(f: () => Promise<T>): Promise<T> {
    await this.lock();
    const res = await f();
    await this.unlock();
    return res;
  }

  public async lock() {
    if (this.locks.length === 0) {
      await this.create();
    }

    this.locks.push(null);
  }

  public async unlock() {
    this.locks.pop();

    if (this.locks.length === 0) {
      await this.loading.dismiss();
      this.loading = undefined;
    }
  }

  private async create() {
    if (!this.loading) {
      this.loading = await this.loadingController.create();
      await this.loading.present();
    }
  }
}
