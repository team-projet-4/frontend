import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy, Router } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { StateService } from "@services/state/state.service";
import { ToasterService } from "@services/frontend/toaster.service";
import { Routes } from "@services/api/route.manager";
import {
  AdminGuard,
  UserGuard,
  TeacherGuard,
  UserGuardMBTI,
} from "@services/frontend/auth.guard";
import { AuthenticationService } from "@services/api/authentication.service";
import { LoadingService } from "@services/frontend/loading.service";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DragulaModule } from "ng2-dragula";
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DragulaModule.forRoot(),
  ],
  providers: [
    ToasterService,
    LoadingService,
    {
      provide: Routes,
      useFactory: (t: ToasterService, l: LoadingService) => new Routes(t, l),
      deps: [ToasterService, LoadingService],
    },
    StateService,
    AuthenticationService,
    AdminGuard,
    TeacherGuard,
    UserGuard,
    UserGuardMBTI,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
