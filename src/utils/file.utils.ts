import * as converter from "csvtojson";
import { isArray } from "polyteam-shared";

export class FileUtils {
  public static readAsText(file: File) {
    return new Promise((resolve: (content: string) => void, _) => {
      const reader = new FileReader();
      reader.onload = (ev: any) => {
        resolve(ev.target.result as string);
      };
      reader.readAsText(file);
    });
  }

  public static async csvToJson<T>(
    csv: string,
    v?: (o: any) => o is T
  ): Promise<[boolean, T[]]> {
    const res = await converter().fromString(csv);
    return v ? [isArray(res, v), res] : [true, res];
  }
}
