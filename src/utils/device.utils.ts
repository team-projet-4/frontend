const REGEX_MOBILE = new RegExp(
  /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/,
  "i"
);

export class DeviceUtils {
  static get isMobile() {
    return REGEX_MOBILE.test(window.navigator.userAgent);
  }
}
