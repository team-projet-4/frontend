import { IEnvironement } from "./environment.model";

export const environment: IEnvironement = {
  production: true,
  BACKEND_URL: "http://localhost:3000",
};
