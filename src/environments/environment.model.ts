export interface IEnvironement {
  readonly production: boolean;
  readonly BACKEND_URL: string;
}
