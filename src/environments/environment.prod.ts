import { IEnvironement } from "./environment.model";

export const environment: IEnvironement = {
  production: true,
  BACKEND_URL: "https://polyteam-backend-staging.herokuapp.com"
};
