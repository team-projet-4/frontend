FROM node:12

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN npm install -g serve

COPY package.json /usr/src/app/
RUN npm install

COPY . /usr/src/app
RUN npm run build-local

RUN cp www/index.html www/200.html

EXPOSE 80
WORKDIR /usr/src/app/www

CMD [ "serve", "-l", "tcp://0.0.0.0:80" ]